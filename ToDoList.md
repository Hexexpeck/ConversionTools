# To Do List
This is where I'll place things that I need to do with this project.
## To Do -- `LoopingAudioConverter`
-Figure out how to fix a bug where `LoopingAudioConverter` wouldn't convert in the `SplatAIO`
## To Do -- `BRSTMConversionTool`
-Figure out why `BRSTMConversionTool` wouldn't convert some file types to BRSTM form.
## To Do -- `The ConversionTools Repository Itself`
-Make this to do list.
