## How do I use ConversionTools?

### Requirements:

1. Enough disk space
2. This tool

### Steps:

1. Open the step1 folder then run the LoopingAudioConverter.
2. In the LoopingAudioConverter, choose Add.
3. Choose the MP3 file you want to convert.
4. When selected, choose where you want the BRSTM/whatever format you chose to be saved and choose which format you want. Then press Start at the bottom!
5. When it finishes, go to the step1 folder again, then output or wherever you chose to save it.
6. Now you know where the newly made BRSTM file is at.
7. Exit the step1 folder and go to the step2 folder.
8. Run the BRSTMAudioConverter in the step2 folder
9. Choose one of the conversion options at the bottom of BRSTMAudioConverter.
10. Go to the output of the step1 folder and choose the file you just exported from LoopingAudioConverter
11. MAKE SURE THE FILE DOESN'T HAVE SPACES. REPLACE ANY SPACES WITH DASHES OR JUST BUNCH THE NAME TOGETHER.
12. Click Import.
13. When it imports, give it a second to convert.
14. You have now converted a file to BFSTM format!


Use it for whatever! Recommended: MusicRandomizer
